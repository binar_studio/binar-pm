namespace :api do
  desc "API Routes"
  task :routes => :environment do
    API::V1::Base.routes.each do |api|
      puts api
    end
  end
end