class Project < ActiveRecord::Base

  has_many :milestones
  has_many :tasks, through: :milestones
  has_and_belongs_to_many :users, join_table: :users_projects

  accepts_nested_attributes_for :users

  private

  def project_params
    params.require(:name).permit(:name, :description, :customer)
  end
end
