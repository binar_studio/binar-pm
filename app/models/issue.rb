class Issue < ActiveRecord::Base
  belongs_to :project

  private

  def issue_params
    params.require(:title).permit(:title, :body)
  end
end
