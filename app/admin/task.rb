ActiveAdmin.register Task do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  permit_params :content, :due, :milestone_id, :project_id, :done

  sidebar :milestone, :only => :show do
    attributes_table_for task.milestone, :name, :project, :created_at
  end


end
