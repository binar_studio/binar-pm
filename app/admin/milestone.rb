ActiveAdmin.register Milestone do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  permit_params :name, :project_id

  show do

    panel "Milestone details" do
      attributes_table_for milestone, :name, :project, :created_at
    end

    panel "Tasks" do
      table_for(milestone.tasks) do |t|
        t.column("Task") {|item| auto_link item }
        t.column("Due") {|item| item.due }
      end
    end
  end

end
