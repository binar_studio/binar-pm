ActiveAdmin.register Project do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  permit_params :name, :description, :customer, users_ids: []

  show do

    panel "Project details" do
      attributes_table_for project, :name, :description, :customer, :created_at
    end

    panel "Milestones" do
      table_for(project.milestones) do |t|
        t.column("Milestone") {|item| auto_link item }
        t.column("Progress") {|item| (item.tasks.where(:done => true).count / item.tasks.count * 100).to_s + "%" }
      end
    end

    panel "Tasks" do
      table_for(project.tasks) do |t|
        t.column("Task") {|item| auto_link item }
        t.column("Due") {|item| item.due }
      end
    end
  end

  form do |f|
    f.inputs 'Edit Project' do
      f.input :name
      f.input :description
      f.input :customer
      f.input :users, :as => :check_boxes, :input_html => {:multiple => true}, :member_label => :email
    end
    f.actions
  end

  sidebar :team, :only => :show do
      table_for(project.users) do |t|
        t.column("Name") {|item| auto_link item }
      end
  end

end
