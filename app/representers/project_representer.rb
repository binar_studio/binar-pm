class ProjectRepresenter < Roar::Decorator
  include Roar::JSON
  
  property :id  
  property :name  
  property :customer  
  property :description

  collection :milestones, extend: MilestoneRepresenter, class: Milestone
end
