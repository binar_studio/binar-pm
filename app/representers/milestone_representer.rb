class MilestoneRepresenter < Roar::Decorator
  include Roar::JSON
  
  property :id
  property :name  
  property :description

  collection :tasks, extend: TaskRepresenter, class: Task
end
