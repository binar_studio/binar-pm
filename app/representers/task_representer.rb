module TaskRepresenter
  include Roar::JSON
  
  property :id  
  property :content
  property :due
  property :done
end
