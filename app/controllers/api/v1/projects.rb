module API
  module V1
    class Projects < Grape::API
      include API::V1::Defaults

      resource :projects do

        desc "Return list of projects"
        get do
          Project.all
        end

        desc "Return a single project"
        params do
          requires :id, type: Integer, desc: "Project id"
        end
        route_param :id do
          get do
            project = Project.find(params[:id])
            ProjectRepresenter.new(project).to_json()
          end


          resource :members do
            desc "Return members of a project"
            get do
              @project = Project.find(params[:id])
              @project.users
            end
          end

          resource :milestones do
            desc "Return milestones of a project"
            get do
              @project = Project.find(params[:id])
              @project.milestones
            end

            desc "Return a single milestone"
            params do
              requires :milestone_id, type: Integer, desc: "Milestone id"
            end
            route_param :milestone_id do

              get do
                Milestone.find(params[:milestone_id])
              end

              resource :tasks do

                desc "Return tasks of a milestone"
                get do
                  Task.where(project_id: params[:id], milestone_id: params[:milestone_id])
                end
              end

              resource :issues do

                desc "Return issues of a milestone"
                get do
                  Issue.where(project_id: params[:id], milestone_id: params[:milestone_id])
                end
              end

            end

          end

        end

      end
    end
  end
end