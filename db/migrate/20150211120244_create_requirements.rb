class CreateRequirements < ActiveRecord::Migration
  def change
    create_table :requirements do |t|
      t.string :content
      t.references :project, index: true

      t.timestamps null: false
    end
    add_foreign_key :requirements, :projects
  end
end
