class CreateIssues < ActiveRecord::Migration
  def change
    create_table :issues do |t|
      t.string :title
      t.string :body
      t.boolean :resolved
      t.references :milestone, index: true
      t.references :project, index: true

      t.timestamps
    end
    add_foreign_key :issues, :milestones
    add_foreign_key :issues, :projects
  end
end
