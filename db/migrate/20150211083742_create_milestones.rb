class CreateMilestones < ActiveRecord::Migration
  def change
    create_table :milestones do |t|
      t.string :name
      t.string :description
      t.references :project, index: true

      t.timestamps
    end
    add_foreign_key :milestones, :projects
  end
end
