class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :content
      t.datetime :due
      t.boolean :done
      t.references :milestone, index: true
      t.references :project, index: true

      t.timestamps
    end
    add_foreign_key :tasks, :milestones
    add_foreign_key :tasks, :projects
  end
end
