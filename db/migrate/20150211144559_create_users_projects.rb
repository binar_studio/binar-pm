class CreateUsersProjects < ActiveRecord::Migration
  def change
    create_table :users_projects do |t|
      t.references :project, index: true
      t.references :user, index: true
    end
    add_foreign_key :users_projects, :projects
    add_foreign_key :users_projects, :users
  end
end
